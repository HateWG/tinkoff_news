//
//  NewsViewController.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 19/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

protocol ModalViewControllerDelegate: class {
    func refreshContent()
    func disableOfflineMode()
}

class NewsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var postContentLabel: UILabel!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    
    weak var delegate: ModalViewControllerDelegate?
    var postsStorage: [CustomNewsTableViewCellData] = []
    var selectedId: Int!
    var requestSender: RequestSenderProtocol = RequestSender()
    var dataStack: CoreDataStack!
    var inOfflineMode: Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadContent()
        
        
        weak var weakSelf = self
        navigationItem.leftBarButtonItems = CustomBackButton.createWithText(text: "Назад", color: UIColor.yellow, target: weakSelf, action: #selector(NewsViewController.tappedBackButton))
    }
    
    func loadContent() {
        spiner.startAnimating()
        let config = RequestsFactory.GetNewsFromAPI.SinglePostConfig(id: postsStorage[selectedId].id!)
        requestSender.send(config: config) { [weak self] (result) in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    guard let postId = self?.selectedId
                        else {
                            print("something wrong with ID")
                            return
                    }
                    let post = self?.postsStorage[postId]
                    post?.content = data.postText
                    self?.showContent()
                    self?.delegate?.disableOfflineMode()
                }
            case .error(let description):
                print("Some error happend: \(description)")
            }
        }
    }
    

    func showContent(){
        
        titleLabel.text = postsStorage[selectedId].title
        
        self.postsStorage[selectedId].counter! += 1
        
        guard let news = News.findOrInsertNews(in: dataStack.saveContext!, with: postsStorage[selectedId].id!) else {
            print("can't extract or insert current news")
            return
        }
        news.content = postsStorage[selectedId].content
        news.counter = Int16(postsStorage[selectedId].counter!)
        
        dataStack.saveContext?.perform {
            self.dataStack.performSave(context: self.dataStack.saveContext!, completionHandler: nil)
        }
        
        guard let content = postsStorage[selectedId].content else { return }
        
        postContentLabel.text = content.html2String
        
        spiner.stopAnimating()
        spiner.isHidden = true
    }

    
    @objc func tappedBackButton() {
        delegate?.refreshContent()
        self.navigationController!.popViewController(animated: true)
    }
    
    
    static func storyboardInstance() -> NewsViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "NewsViewController") as? NewsViewController
    }
    
    func showAlert() {
        if postsStorage[selectedId].content != nil {
            showContent()
        }
        else {
            DispatchQueue.main.async {
                self.titleLabel.text = "Нет соединения с интернетом."
                self.postContentLabel.text = "Контент не доступен в режиме оффлайн"
                self.spiner.stopAnimating()
                self.spiner.isHidden = true
                print("no no no")
            }
        }
    }
    
}


extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                            .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error:", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

