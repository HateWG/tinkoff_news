//
//  Parsers.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 19/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import Foundation

protocol ParserProtocol {
    associatedtype Model
    func parse(data: Data) -> Model?
}

struct NewsListModel {
    let id: Int
    let text: String
}

struct SinglePostModel {
    let postText: String
}

class NewsListParser: ParserProtocol {
    
    typealias model = [NewsListModel]
    var newsStorage : [NewsListModel] = []
    func parse(data: Data) -> [NewsListModel]? {
        do {
            
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                return nil
            }
            
            guard let items = json["payload"] as? [[String: AnyObject]] else {
                return nil
            }
            
            for item in items {
                guard let id = item["id"] as? String else {
                    print("Post hasn't id. -_-")
                    continue
                }
                guard let text = item["text"] as? String else {
                    print("Post hasn't main text. -_-")
                    continue
                }
                
                guard let newID = Int(id) else { return nil }
                newsStorage.append(NewsListModel(id: newID, text: text))
            }
            
            return newsStorage
            
        } catch {
            print("Can't get data from JSON\n\(error)")
            return nil
        }
    }
}

class SinglePostParser: ParserProtocol {
    
    typealias model = SinglePostModel
    
    func parse(data: Data) -> SinglePostModel? {
        do {
            
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                return nil
            }
            
            guard let checker = json["resultCode"] as? String else {
                return nil
            }
            print(checker)
            
            guard let item = json["payload"] as? [String: AnyObject] else {
                return nil
            }
            
            guard let content = item["content"] as? String else {
                print("Post hasn't content at all. -_-")
                return nil
            }
            
            return SinglePostModel(postText: content)
        }
        catch
        {
            print("Can't get data from JSON\n\(error)")
            return nil
        }
        
    }
}
