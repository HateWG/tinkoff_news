//
//  RequestSender.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 19/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(String)
}

protocol RequestSenderDelegateProtocol: class {
    func showAlert()
}

protocol RequestSenderProtocol {
    var requestDelegate: RequestSenderDelegateProtocol? {get set}
    func send<Model, Parser>(config: RequestConfig<Model, Parser>, completionHandler: @escaping (Result<Model>) -> Void )
}

class RequestSender: RequestSenderProtocol {
    weak var requestDelegate: RequestSenderDelegateProtocol?
    let session = URLSession.shared
    let queue: DispatchQoS.QoSClass = .userInitiated
    var async: Bool
    
    init() {
        self.async = true
    }
    
    
    func send<ModelType, Parser>(config: RequestConfig<ModelType, Parser>, completionHandler: @escaping (Result<ModelType>) -> Void) {
        
        guard let urlRequest = config.request.urlRequest else {
            completionHandler(Result.error("URL string can't be parsed to URL"))
            return
        }
        
        let task = session.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                completionHandler(Result.error(error.localizedDescription))
                DispatchQueue.main.async {
                    
                    self.requestDelegate?.showAlert()
                }
                return
            }
            guard let data = data,
                let parsedModel = config.parser.parse(data: data) else {
                    completionHandler(Result.error("Received data can not be parsed"))
                    return
            }
            
            completionHandler(Result.success(parsedModel))
        }
        
        if async {
            let queue = self.queue
            DispatchQueue.global(qos: queue).async {
                task.resume()
            }
        }
    }
}
