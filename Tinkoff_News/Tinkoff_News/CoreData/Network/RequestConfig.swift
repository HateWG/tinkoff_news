//
//  RequestConfig.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 19/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import Foundation

struct RequestConfig<Model, Parser: ParserProtocol> where Parser.Model == Model {
    let request: RequestProtocol
    let parser: Parser
}
