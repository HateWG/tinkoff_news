//
//  RequestFactory.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 19/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import Foundation

class RequestsFactory {
    
    struct GetNewsFromAPI {
        //configuration to get list of news
        static func NewsListConfig(from: Int = 0, to: Int = 20) -> RequestConfig<[NewsListModel], NewsListParser> {
            return RequestConfig(request: NewsListRequest(from: from, to: to), parser: NewsListParser())
        }
        // configuration to get a single news post
        static func SinglePostConfig(id: Int) -> RequestConfig<SinglePostModel, SinglePostParser> {
            return RequestConfig(
                request: SinglePostRequest(id: id), parser: SinglePostParser())
        }
    }
}
