//
//  NewsTableViewCell.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 16/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    var title: String? {
        didSet {
           titleLabel.text = title
        }
    }
    
    
    @IBOutlet weak var counterLabel: UILabel!
    var counter: Int = 0 {
        didSet {
            counterLabel.text = String(counter)
        }
    }
}
