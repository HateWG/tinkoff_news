//
//  ViewController.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 16/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import UIKit

class NewsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, RequestSenderDelegateProtocol, ModalViewControllerDelegate {
    
    @IBOutlet weak var newsTableView: UITableView!
    
    
    
    
    let cellId = "cell"
    var fullPostsStorage: [CustomNewsTableViewCellData] = []
    var requestSender: RequestSenderProtocol = RequestSender()
    var coreDataStack = CoreDataStack()
    
    var refreshControl: UIRefreshControl!
    
    var selectedPost: Int!
    var from: Int = 0
    var to: Int = 20
    var inOfflineMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newsTableView.delegate = self
        self.newsTableView.dataSource = self
        self.requestSender.requestDelegate = self
        
        navigationBarStyle()
        setRefreshControl()
        
        getNewsList(update: false)
    }
    
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fullPostsStorage.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedPost = indexPath.row
        
        if let viewController = NewsViewController.storyboardInstance() {
            viewController.delegate = self
            viewController.modalPresentationStyle = .overFullScreen
            viewController.postsStorage = fullPostsStorage
            viewController.selectedId = selectedPost
            viewController.dataStack = coreDataStack
            viewController.inOfflineMode = inOfflineMode
            
        navigationController?.pushViewController(viewController, animated: true)
        }
        
        print(from)
        print(to)
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! NewsTableViewCell
        
        let post = fullPostsStorage[indexPath.row]
        
        
        if indexPath.row != fullPostsStorage.count - 1{
            cell.title = post.title ?? "nil"
            cell.counter = post.counter ?? 0
            
            
        }else{
            if inOfflineMode == false {
                getNewsList(update: false)
                cell.title = post.title ?? "nil"
                cell.counter = post.counter ?? 0
                
            } else {
                print("it's offline mode and there is no more posts over there")
            }
        }
        return cell
    }
    
    
    func setRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        newsTableView.refreshControl = refreshControl
    }
    
    @objc func refresh(refreshControl: UIRefreshControl) {
        getNewsList(update: true)
        let navBarHeight = (self.navigationController?.navigationBar.frame.size.height)!/2.5
        let navBarWidth = self.navigationController?.navigationBar.frame.size.width
        self.newsTableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.refreshControl.endRefreshing()
            self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: navBarWidth!, height: navBarHeight)
        }
    }
    
    func navigationBarStyle()
    {
        self.navigationItem.title = "Tinkoff News"
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
    func getNewsList(update: Bool) {
        if fullPostsStorage.count == 0{
            self.from = 0
            self.to = 20
        }
        if update{
            self.fullPostsStorage = []
            self.from = 0
            self.to = 20
            print("Manual update is started")
        }
        let config = RequestsFactory.GetNewsFromAPI.NewsListConfig(from: from, to: to)
        requestSender.send(config: config) { [weak self] (result) in
            switch result {
            case .success(let data):
                // rewrite with perform and read/write functions
                self?.inOfflineMode = false
                guard let saveContext = self?.coreDataStack.saveContext else {
                    fatalError("Core data stack save context is nil!")
                }
                DispatchQueue.main.async {
                    for items in data
                    {
                        let singlePost = CustomNewsTableViewCellData(id: items.id, title: items.text, content: nil, counter: 0)
                        guard let news = News.findOrInsertNews(in: saveContext, with: singlePost.id!) else {
                            print("can't extract or insert current news")
                            return
                        }
                        if news.title == nil{
                            news.title = singlePost.title
                            news.counter = Int16(singlePost.counter!)

                            
                            self?.fullPostsStorage.append(singlePost)
                        } else{
                            
                            singlePost.counter = Int(news.counter)
                            singlePost.content = news.content
                            
                            self?.fullPostsStorage.append(singlePost)
                        }
                        
                    }
                    
                    self?.coreDataStack.performSave(context: saveContext, completionHandler: nil)
                    self?.newsTableView.reloadData()
                    self?.from += 20
                    self?.to += 20
                    
                }
            case .error(let description):
                print("Some error happend: \(description)")
                
            }
        }
    }
    func refreshContent() {
        DispatchQueue.main.async {
            self.newsTableView.reloadData()
        }
    }
    
    func disableOfflineMode() {
        DispatchQueue.main.async {
            if self.inOfflineMode == true {
                self.inOfflineMode = false
                self.getNewsList(update: true)
                self.newsTableView.reloadData()
            }
        }
    }
    
    func showAlert() {
        DispatchQueue.main.async {
            
            self.coreDataStack.saveContext?.perform {
                let news = News.findNews(in: self.coreDataStack.saveContext!)
                if news.count == 0 {
                    let emptyCoreDataAlert = UIAlertController(title: "Отсутствие локальных записей", message: "Для получения данных подключитесь к сети.", preferredStyle: UIAlertController.Style.alert)
                    emptyCoreDataAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(emptyCoreDataAlert, animated: true, completion: nil)
                    
                }
                else {
                    self.inOfflineMode = true
                    print("локальное хранилище не пустое! тут  \(news.count) записей")
                    print("первый элемент с верху \(String(describing: news[0]?.id))")
                    
                    if self.fullPostsStorage.count == 0{
                        let internetFailAlert = UIAlertController(title: "Оффлайн режим", message: "Отсутствует подключение к сети.", preferredStyle: UIAlertController.Style.alert)
                        internetFailAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(internetFailAlert, animated: true, completion: nil)
                        
                        for item in news{
                            guard let intID = item?.id else { return }
                            guard let intCounter = item?.counter else { return }
                            self.fullPostsStorage.append(CustomNewsTableViewCellData(id: Int(intID),
                                                                                     title: item?.title,
                                                                                     content: item?.content,
                                                                                     counter: Int(intCounter)
                            ))
                        }
                        DispatchQueue.main.async {
                            self.newsTableView.reloadData()
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            let reloadAlert = UIAlertController(title: "Интернет соединение потеряно!", message: "Обновите таблицу для доступа к оффлайн режиму", preferredStyle: UIAlertController.Style.alert)
                            self.present(reloadAlert, animated: true, completion: nil)
                            
                            let when = DispatchTime.now() + 2.5
                            DispatchQueue.main.asyncAfter(deadline: when){
                                reloadAlert.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
}

