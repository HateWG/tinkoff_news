//
//  NewsTableViewCellData.swift
//  Tinkoff_News
//
//  Created by Томас Димеджи Акинделе Ало on 17/05/2019.
//  Copyright © 2019 Томас Димеджи Акинделе Ало. All rights reserved.
//

import Foundation

protocol NewsListCellProtocol: class{
    var id: Int? {get set}
    var title: String? {get set}
    var content: String? {get set}
    var counter: Int? {get set}
}

class CustomNewsTableViewCellData : NewsListCellProtocol{
    var id: Int?
    var title: String?
    var content: String?
    var counter: Int?
    
    init(id: Int?, title: String?, content: String?, counter: Int? = 0)
    {
        self.id = id
        self.title = title
        self.content = content
        self.counter = counter
    }
}
